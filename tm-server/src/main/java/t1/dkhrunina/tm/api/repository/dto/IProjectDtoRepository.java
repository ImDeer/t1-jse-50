package t1.dkhrunina.tm.api.repository.dto;

import t1.dkhrunina.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

}