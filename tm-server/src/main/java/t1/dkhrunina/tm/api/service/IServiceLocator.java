package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.service.dto.*;
import t1.dkhrunina.tm.api.service.model.*;

public interface IServiceLocator {

    @NotNull
    IAuthDtoService getAuthDtoService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectDtoService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskDtoService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskDtoService getTaskDtoService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserDtoService getUserDtoService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}