package t1.dkhrunina.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.model.Session;

import java.util.List;

public interface ISessionService extends IUserOwnedService<Session> {

    Session remove(@Nullable Session session);

    List<Session> findAll();

}