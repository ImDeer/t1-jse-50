package t1.dkhrunina.tm.log;

public enum OperationType {

    INSERT,
    DELETE,
    UPDATE
}