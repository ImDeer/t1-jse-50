package t1.dkhrunina.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@NoArgsConstructor
public class JmsLoggerProducer {

    @NotNull
    private final String QUEUE = "TM_QUEUE";

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private ConnectionFactory connectionFactory;

    @NotNull
    private Connection connection;

    @NotNull
    private Session session;

    @NotNull
    private Queue destination;

    @NotNull
    private MessageProducer messageProducer;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @SneakyThrows
    public JmsLoggerProducer(@NotNull final IPropertyService propertyService) {
        @NotNull final String activeMQHost = propertyService.getActiveMQHost();
        @NotNull final String url = "tcp://" + activeMQHost + ":61616";
        connectionFactory = new ActiveMQConnectionFactory(url);
        broker.addConnector(url);
        broker.start();
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        @NotNull final TextMessage textMessage = session.createTextMessage(text);
        messageProducer.send(textMessage);
    }

    public void send(@NotNull final OperationEvent operationEvent) {
        executorService.submit(() -> sync(operationEvent));
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent operationEvent) {
        @NotNull final Class<?> entityClass = operationEvent.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            operationEvent.setTable(table.name());
        }
        send(objectMapper.writeValueAsString(operationEvent));
    }

}