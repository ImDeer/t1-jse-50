package t1.dkhrunina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.endpoint.IDomainEndpoint;
import t1.dkhrunina.tm.api.service.IServiceLocator;
import t1.dkhrunina.tm.dto.request.data.*;
import t1.dkhrunina.tm.dto.response.data.*;
import t1.dkhrunina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "t1.dkhrunina.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBase64Response loadBase64Data(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadBase64Request request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataLoadBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBinaryResponse loadBinaryData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataLoadBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonFasterXmlResponse loadJsonFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataLoadJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonJaxBResponse loadJsonJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadJsonJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxB();
        return new DataLoadJsonJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlFasterXmlResponse loadXmlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataLoadXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlJaxBResponse loadXmlJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadXmlJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxB();
        return new DataLoadXmlJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadYamlFasterXmlResponse loadYamlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataLoadYamlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBase64Response saveBase64Data(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveBase64Request request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataSaveBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBinaryResponse saveBinaryData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataSaveBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonFasterXmlResponse saveJsonFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataSaveJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonJaxBResponse saveJsonJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveJsonJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxB();
        return new DataSaveJsonJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlFasterXmlResponse saveXmlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataSaveXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlJaxBResponse saveXmlJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveXmlJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxB();
        return new DataSaveXmlJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveYamlFasterXmlResponse saveYamlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXml();
        return new DataSaveYamlFasterXmlResponse();
    }

}