package t1.dkhrunina.tm.repository.dto;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.dto.IUserDtoRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;
import t1.dkhrunina.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserDtoRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private static EntityManager entityManager;

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private IUserDtoRepository userRepository;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        userRepository = new UserDtoRepository(entityManager);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("user" + i);
            user.setEmail("user" + i + "@email.ru");
            user.setFirstName("name" + i);
            user.setLastName("lastname" + i);
            user.setPasswordHash(HashUtil.salt(new PropertyService(), "meow" + i));
            userRepository.add(user);
            userList.add(user);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final String userFirstName = "name";
        @NotNull final String userLastName = "last name";
        @NotNull final String userLogin = "login";
        @NotNull final String userEmail = "email@email.ru";
        @NotNull final UserDTO user = new UserDTO();
        user.setFirstName(userFirstName);
        user.setLastName(userLastName);
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash("meow");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final UserDTO createdUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(userFirstName, createdUser.getFirstName());
        Assert.assertEquals(userLastName, createdUser.getLastName());
        Assert.assertEquals(userLogin, createdUser.getLogin());
        Assert.assertEquals(userEmail, createdUser.getEmail());
        userRepository.remove(createdUser);
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = userRepository.getSize() + 2;
        @NotNull final List<UserDTO> users = new ArrayList<>();
        @NotNull final String firstUserFirstName = "udto name 1";
        @NotNull final String firstUserLastName = "udto last name 1";
        @NotNull final String firstUserLogin = "udto login 1";
        @NotNull final String firstUserEmail = "udto1@email.ru";
        @NotNull final UserDTO firstUser = new UserDTO();
        firstUser.setFirstName(firstUserFirstName);
        firstUser.setLastName(firstUserLastName);
        firstUser.setLogin(firstUserLogin);
        firstUser.setEmail(firstUserEmail);
        firstUser.setPasswordHash("meow");
        users.add(firstUser);
        @NotNull final String secondUserFirstName = "udto name 2";
        @NotNull final String secondUserLastName = "udto last name 2";
        @NotNull final String secondUserLogin = "udto login 2";
        @NotNull final String secondUserEmail = "udto2@email.ru";
        @NotNull final UserDTO secondUser = new UserDTO();
        secondUser.setFirstName(secondUserFirstName);
        secondUser.setLastName(secondUserLastName);
        secondUser.setLogin(secondUserLogin);
        secondUser.setEmail(secondUserEmail);
        secondUser.setPasswordHash("meow");
        users.add(secondUser);
        @NotNull final Collection<UserDTO> addedUserDTOs = userRepository.add(users);
        Assert.assertTrue(addedUserDTOs.size() > 0);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
        userRepository.remove(firstUser);
        userRepository.remove(secondUser);
    }

    @Test
    public void testFindAll() {
        @Nullable final List<UserDTO> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    public void testFindByLogin() {
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final UserDTO foundUserDTO = userRepository.findByLogin(login);
            Assert.assertEquals(user, foundUserDTO);
        }
    }

    @Test
    public void testFindByLoginNull() {
        @Nullable final UserDTO foundUserDTONull = userRepository.findByLogin(null);
        Assert.assertNull(foundUserDTONull);
        @Nullable final UserDTO foundUserDTO = userRepository.findByLogin("meow");
        Assert.assertNull(foundUserDTO);
    }

    @Test
    public void testFindByEmail() {
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final UserDTO foundUserDTO = userRepository.findByEmail(email);
            Assert.assertEquals(user, foundUserDTO);
        }
    }

    @Test
    public void testFindByEmailNull() {
        @Nullable final UserDTO foundUserDTONull = userRepository.findByEmail(null);
        Assert.assertNull(foundUserDTONull);
        @Nullable final UserDTO foundUserDTO = userRepository.findByEmail("meow");
        Assert.assertNull(foundUserDTO);
    }

    @Test
    public void testFindOneById() {
        @Nullable UserDTO user;
        for (int i = 0; i < userList.size(); i++) {
            user = userList.get(i);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final UserDTO foundUserDTO = userRepository.findOneById(userId);
            Assert.assertNotNull(foundUserDTO);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final UserDTO foundUserDTO = userRepository.findOneById("meow");
        Assert.assertNull(foundUserDTO);
        @Nullable final UserDTO foundUserDTONull = userRepository.findOneById(null);
        Assert.assertNull(foundUserDTONull);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 1; i <= userList.size(); i++) {
            @Nullable final UserDTO user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    public void testFindOneByIndexNull() {
        @Nullable final UserDTO user = userRepository.findOneByIndex(null);
        Assert.assertNull(user);
    }

    @Test
    public void testGetSize() {
        int actualSize = userRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    public void testRemove() {
        @Nullable final UserDTO user = userList.get(1);
        Assert.assertNotNull(user);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO deletedUserDTO = userRepository.remove(user);
        Assert.assertNotNull(deletedUserDTO);
        @Nullable final UserDTO deletedUserDTOInRepository = userRepository.findOneById(userId);
        Assert.assertNull(deletedUserDTOInRepository);
    }

    @Test
    public void testRemoveNull() {
        @Nullable final UserDTO user = userRepository.remove(null);
        Assert.assertNull(user);
    }

}