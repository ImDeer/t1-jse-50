package t1.dkhrunina.tm.migration;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.dto.ISessionDtoRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.SessionDTO;
import t1.dkhrunina.tm.repository.dto.SessionDtoRepository;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;

public class SessionSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        liquibase.update("session");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        @Nullable final SessionDTO sessionDTO = createSession();
        Assert.assertNotNull(sessionDTO);
        Assert.assertEquals(1, getCountOfSessions());
        deleteProject(sessionDTO);
    }

    @NotNull
    private SessionDTO createSession() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionDtoRepository sessionDtoRepository = new SessionDtoRepository(entityManager);
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDtoRepository.add(sessionDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return sessionDTO;
    }

    private int getCountOfSessions() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionDtoRepository sessionDtoRepository = new SessionDtoRepository(entityManager);
        final int count = sessionDtoRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteProject(@NotNull final SessionDTO sessionDTO) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionDtoRepository sessionDtoRepository = new SessionDtoRepository(entityManager);
        sessionDtoRepository.remove(sessionDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}