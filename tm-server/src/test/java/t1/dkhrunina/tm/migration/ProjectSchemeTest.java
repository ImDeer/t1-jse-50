package t1.dkhrunina.tm.migration;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.dto.IProjectDtoRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.repository.dto.ProjectDtoRepository;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;

public class ProjectSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        liquibase.update("project");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        @Nullable final ProjectDTO projectDTO = createProject();
        Assert.assertNotNull(projectDTO);
        Assert.assertEquals(1, getCountOfProjects());
        deleteProject(projectDTO);
    }

    @NotNull
    private ProjectDTO createProject() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectDtoRepository projectDtoRepository = new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDtoRepository.add(projectDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projectDTO;
    }

    private int getCountOfProjects() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectDtoRepository projectDtoRepository = new ProjectDtoRepository(entityManager);
        final int count = projectDtoRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteProject(@NotNull final ProjectDTO projectDTO) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectDtoRepository projectDtoRepository = new ProjectDtoRepository(entityManager);
        projectDtoRepository.remove(projectDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}