package t1.dkhrunina.tm.component;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.IPropertyService;
import t1.dkhrunina.tm.api.IReceiverService;
import t1.dkhrunina.tm.listener.EntityListener;
import t1.dkhrunina.tm.service.PropertyService;
import t1.dkhrunina.tm.service.ReceiverService;

public class Bootstrap {

    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final String activeMQHost = propertyService.getActiveMQHost();
        @NotNull final String activeMQPort = propertyService.getActiveMQPort();
        @NotNull final String Url = "tcp://" + activeMQHost + ":" + activeMQPort;
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(Url);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new EntityListener(propertyService));
    }

}