package t1.dkhrunina.tm.service;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String ACTIVE_MQ_HOST_KEY = "activeMQ.host";

    @NotNull
    public static final String ACTIVE_MQ_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String ACTIVE_MQ_PORT_KEY = "activeMQ.port";

    @NotNull
    public static final String ACTIVE_MQ_PORT_DEFAULT = "61616";

    @NotNull
    public static final String MONGO_CLIENT_HOST_KEY = "mongo.client.host";

    @NotNull
    public static final String MONGO_CLIENT_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String MONGO_CLIENT_PORT_KEY = "mongo.client.port";

    @NotNull
    public static final String MONGO_CLIENT_PORT_DEFAULT = "localhost";

    @NotNull
    private static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    private static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    @SuppressWarnings("IOStreamConstructor")
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @Override
    @NotNull
    public String getActiveMQHost() {
        return getStringValue(ACTIVE_MQ_HOST_KEY, ACTIVE_MQ_HOST_DEFAULT);
    }

    @Override
    @NotNull
    public String getActiveMQPort() {
        return getStringValue(ACTIVE_MQ_PORT_KEY, ACTIVE_MQ_PORT_DEFAULT);
    }

    @Override
    @NotNull
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @Override
    @NotNull
    public String getMongoClientHost() {
        return getStringValue(MONGO_CLIENT_HOST_KEY, MONGO_CLIENT_HOST_DEFAULT);
    }

    @Override
    @NotNull
    public String getMongoClientPort() {
        return getStringValue(MONGO_CLIENT_PORT_KEY, MONGO_CLIENT_PORT_DEFAULT);
    }

}
