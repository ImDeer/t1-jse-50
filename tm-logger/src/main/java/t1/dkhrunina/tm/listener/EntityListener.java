package t1.dkhrunina.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.ILoggerService;
import t1.dkhrunina.tm.api.IPropertyService;
import t1.dkhrunina.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@NoArgsConstructor
public class EntityListener implements MessageListener {

    private ILoggerService loggerService;

    public EntityListener(@NotNull final IPropertyService propertyService) {
        loggerService = new LoggerService(propertyService);
    }

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        final TextMessage textMessage = (TextMessage) message;
        final String text = textMessage.getText();
        loggerService.log(text);
    }

}