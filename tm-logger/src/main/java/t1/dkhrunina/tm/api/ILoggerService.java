package t1.dkhrunina.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull String message);

}