package t1.dkhrunina.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.enumerated.Role;

public interface ICommand {

    void execute();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @NotNull
    String getName();

    @Nullable
    Role[] getRoles();

}