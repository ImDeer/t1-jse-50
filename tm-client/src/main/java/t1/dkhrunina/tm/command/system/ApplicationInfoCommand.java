package t1.dkhrunina.tm.command.system;

import org.jetbrains.annotations.NotNull;

import static t1.dkhrunina.tm.util.FormatUtil.formatBytes;

public final class ApplicationInfoCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    private static final String NAME = "info";

    @NotNull
    private static final String DESCRIPTION = "Show system info.";

    @Override
    public void execute() {
        System.out.println("\n[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Processors: " + processorCount);
        System.out.println("Max memory: " + formatBytes(maxMemory));
        System.out.println("Total memory: " + formatBytes(totalMemory));
        System.out.println("Free memory: " + formatBytes(freeMemory));
        System.out.println("Used memory: " + formatBytes(usedMemory));
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}