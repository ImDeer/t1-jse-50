package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataLoadYamlFasterXmlRequest;

public final class DataLoadYamlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-yaml-faster";

    @NotNull
    private static final String DESCRIPTION = "Load data from FasterXML YAML file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().loadYamlFasterXmlData(new DataLoadYamlFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}