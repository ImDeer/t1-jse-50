package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.project.ProjectClearRequest;
import t1.dkhrunina.tm.dto.response.project.ProjectClearResponse;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        System.out.println("[Clear project list]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        @NotNull final ProjectClearResponse response = getProjectEndpoint().clearProject(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}